import 'package:leetcode_problem_solving/add_binary.dart';
import 'package:leetcode_problem_solving/add_digits.dart';
import 'package:leetcode_problem_solving/contains_duplicate.dart';
import 'package:leetcode_problem_solving/find_the_difference.dart';
import 'package:leetcode_problem_solving/first_unique_character_in_a_string.dart';
import 'package:leetcode_problem_solving/fizz_buzz.dart';
import 'package:leetcode_problem_solving/integer_to_roman.dart';
import 'package:leetcode_problem_solving/jewels_and_stones.dart';
import 'package:leetcode_problem_solving/length_of_last_word.dart';
import 'package:leetcode_problem_solving/merge_sorted_lists.dart';
import 'package:leetcode_problem_solving/ransom_note.dart';
import 'package:leetcode_problem_solving/remove_duplicates.dart';
import 'package:leetcode_problem_solving/remove_duplicates_from_sorted_array.dart';
import 'package:leetcode_problem_solving/remove_element.dart';
import 'package:leetcode_problem_solving/reverse_string.dart';
import 'package:leetcode_problem_solving/roman_to_integer.dart';
import 'package:leetcode_problem_solving/search_insert.dart';
import 'package:leetcode_problem_solving/single_number.dart';
import 'package:leetcode_problem_solving/square_root.dart';
import 'package:leetcode_problem_solving/str_str.dart';
import 'package:leetcode_problem_solving/valid_parentheses.dart';
import 'package:leetcode_problem_solving/search_insert.dart';

void main(List<String> arguments) {
  /*
  print ("412. Fizz Buzz (LeetCode)"); 
  print ("-------------------------\n");
  FizzBuzz fizzBuzz = FizzBuzz();
  fizzBuzz.result(15);
  print ("\n\n");

  print ("58. Length of Last Word (LeetCode)"); 
  print ("-------------------------\n");
  LengthOfLastWord  lengthOfLastWord = LengthOfLastWord();
  lengthOfLastWord.result("luffy is still joyboy");
  print ("\n\n");

  print ("13. Roman to Integer (LeetCode)"); 
  print ("-------------------------\n");
  RomanToInteger romanToInteger = RomanToInteger();
  romanToInteger.result("MMMCMXCIX");
  print ("\n\n");

  print ("12. Integer to Roman (LeetCode)"); 
  print ("-------------------------\n");
  IntegerToRoman integerToRoman = IntegerToRoman();
  String value = integerToRoman.result(1994);
  print ("Roman of $num = $value");
  print ("\n\n");
  
  print ("20. Valid Parentheses (LeetCode)"); 
  print ("-------------------------\n");
  ValidParentheses validParentheses = ValidParentheses();
  bool isValid = validParentheses.result("{()}");
  print (isValid? "Input is VALID":"Input is NOT Valid");
  print ("\n\n");

  print ("217. Contains Duplicate (LeetCode)"); 
  print ("-------------------------\n");
  ContainsDuplicate containsDuplicate = ContainsDuplicate();
  bool hasDuplicate = containsDuplicate.result([1,1,1,3,3,4,3,2,4,2]);
  print (hasDuplicate? "Input has duplicates":"Input does not have duplicates");
  print ("\n\n");

  print ("383. Ransom Note (LeetCode)"); 
  print ("-------------------------\n");
  RansomNote ransomNote = RansomNote();
  String note = "ag";
  String magazine = "baba";
  bool canConstruct = ransomNote.canConstruct(note,magazine);
  print (canConstruct? "Can construct":"Cannot construct");
  print ("$note From $magazine");
  print ("\n\n");

  print ("28. Implement strStr (LeetCode)"); 
  print ("-------------------------\n");
  StrStr strStr = StrStr();
  int index = strStr.strStr("hello","ll");
  print ("First occurance: $index");
  print ("\n\n");

  print ("258. Add Digits (LeetCode)"); 
  print ("-------------------------\n");
  AddDigits addDigits = AddDigits();
  print ("Sum of Digits is: ${addDigits.add(382)}");
  print ("\n\n");

  print ("67. Add Binary (LeetCode)"); 
  print ("-------------------------\n");
  AddBinary addBinary = AddBinary();
  print ("Sum of Binary is: ${addBinary.add("1010","1011")}");
  print ("\n\n");

  print ("387. First Unique Character in a String (LeetCode)"); 
  print ("-------------------------\n");
  FirstUniqueCharacter firstUniqueCharacter = FirstUniqueCharacter();
  String text = "aabb";
  print ("Index of first unique character in $text is: ${firstUniqueCharacter.getIndex(text)}");
  print ("\n\n");

  print ("771. Jewels and Stones (LeetCode)"); 
  print ("-------------------------\n");
  JewelsAndStones jewelsAndStones = JewelsAndStones();
  print ("Number of Jewels is: ${jewelsAndStones.getCount("aA", "aAAbbbb")}");
  print ("\n\n");

  print ("389. Find the Difference (LeetCode)"); 
  print ("-------------------------\n");
  FindTheDifference findTheDifference = FindTheDifference();
  print ("Difference is: ${findTheDifference.getDifference("", "y")}");
  print ("\n\n");
  
  print ("344. Reverse String (LeetCode)"); 
  print ("-------------------------\n");
  ReverseString reverseString = ReverseString();
  reverseString.reverse(["H","a","n","n","a","h"]);
  print ("\n\n");


  print ("26. Remove Duplicates from Sorted Array (LeetCode)"); 
  print ("-------------------------\n");
  RemoveDuplicatesFromSortedArray removeDuplicatesFromSortedArray = RemoveDuplicatesFromSortedArray();
  removeDuplicatesFromSortedArray.removeDuplicates([-1,0,0,0,0,3,3]);
  print ("\n\n");

  print ("35. Search Insert Position (LeetCode)"); 
  print ("-------------------------\n");
  SearchInsertPosition searchInsertPosition = SearchInsertPosition();
  int index = searchInsertPosition.searchInsert([1,3,5,6],2);
  print ("Index Is: $index");
  print ("\n\n");


  print ("136. Single Number (LeetCode)"); 
  print ("-------------------------\n");
  SingleNumber singleNumber = SingleNumber();
  int unique = singleNumber.findUnique([1]);
  print ("Unique number is: $unique");
  print ("\n\n");


  print ("21. Merge Two Sorted Lists (LeetCode)"); 
  print ("-------------------------\n");
  MergeLists mergeLists = MergeLists();
  mergeLists.mergeTwoLists();
  print ("\n\n");


  print ("27. Remove Element (LeetCode)"); 
  print ("-------------------------\n");
  RemoveElement removeElement = RemoveElement();
  print("Count of elements after removing target is: ${removeElement.removeSpecificElement([3,2,2,3],3)}");
  print ("\n\n");

  print ("83. Remove Duplicate (LeetCode)"); 
  print ("-------------------------\n");
  RemoveDuplicates removeDuplicates = RemoveDuplicates();
  removeDuplicates.deleteDuplicates();
  print ("\n\n");
  */

  print ("69. Sqrt(x) (LeetCode)"); 
  print ("-------------------------\n");
  SquareRoot squareRoot = SquareRoot();
  int number = 8;
  print("The square root of: $number = ${squareRoot.mySqrt(number)}");
  print ("\n\n");

}
