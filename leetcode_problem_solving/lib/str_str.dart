class StrStr{
   int strStr(String haystack, String needle) {
        if (haystack.isEmpty || needle.isEmpty){
          return 0;
        }
        if (haystack.length < needle.length){
          return -1;
        }
        for (int i = 0; i < haystack.length+1-needle.length; i++){
            for (int j = 0; j < needle.length; j++){
              if (needle[j] != haystack[i+j]){
                break;
              }
              if (j == needle.length -1){
                return i;
              }
            }
        }
        return -1;
    }
}