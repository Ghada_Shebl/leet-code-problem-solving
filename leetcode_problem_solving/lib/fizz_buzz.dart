class FizzBuzz {
    List<String> result(int n) {
        List <String> result = List.empty(growable: true);
        for (int i = 1; i <= n; i++){
          String value = "";
          if (i % 3 == 0){
            value += "Fizz";
          }
          if (i % 5 == 0){
            value += "Buzz";
          }
          result.add(value.isEmpty?i.toString():value);
        }
        print (result.toString());
        return result;
    }
}
