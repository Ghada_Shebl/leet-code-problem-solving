class IntegerToRoman{
    String result(int num) {
        String number = "";
        int temp = 0;
        Map <int,String> map = {
          1000 :"M",
          900 : "CM",
          500 : "D",
          400 : "CD",
          100 : "C",
          90 : "XC",
          50 : "L",
          40 : "XL",
          10 : "X",
          9 : "IX",
          5 : "V",
          4 : "IV",
          1 : "I",
        };

        while (num!=0){
          for (int i = 0; i < map.length; i++){
            if (map.keys.elementAt(i) <= num){
              temp = (num ~/ map.keys.elementAt(i));
              num = (num % (temp*map.keys.elementAt(i)));
              for (int j = 0; j < temp; j++){
                String ?output = map[map.keys.elementAt(i)];
                if (output != null){
                  number+=output;
                }
              }
            }
          }
        }
        return number;
    }
}
