import 'package:stack/stack.dart';

class ValidParentheses{
    bool result(String s) {
        Map <String,String> map = {
          ")":"(",
          "}":"{",
          "]":"["
        };
        Stack <String> stack = Stack();
        for (int i = 0; i < s.length; i++){
          if (map.containsValue(s[i])){
            stack.push(s[i]);
            continue;
          }
          if (stack.isEmpty){
            return false;
          }
          if (map[s[i]] == stack.top()){
            stack.pop();
            continue;
          }
          return false;

        }
        return stack.isEmpty;
    }
}