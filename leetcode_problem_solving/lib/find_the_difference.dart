import 'dart:collection';

class FindTheDifference{
  String getDifference(String s, String t){
    HashMap <String,int> map = HashMap();
    for (int i = 0; i < s.length; i++){
      if (map.containsKey(s[i])){
        map.putIfAbsent(s[i], () => map[s[i]]!+1);
        continue;
      }
      map.putIfAbsent(s[i], () => 1);
    }

    for (int i = 0; i < t.length; i++){
      if (!map.containsKey(t[i])){
        return t[i];
      }
      if (map[t[i]]==1){
        map.remove(t[i]);
        continue;
      }
      map.putIfAbsent(t[i], () => map[t[i]]!-1);
    }
    return "";
  }
}