import 'dart:collection';

class RemoveDuplicatesFromSortedArray{
   int removeDuplicates(List<int>nums){
    LinkedHashSet<int> set = LinkedHashSet();
    for (int i = 0; i < nums.length; i++){
      if (set.contains(nums[i])){
        continue;
      }
      set.add(nums[i]);
    }
    for (int i = 0; i < set.length; i++){
      nums[i] = set.elementAt(i);
    }

    print ("Array after removing elements: ${nums.toString()}");
    return set.length;
   }
}