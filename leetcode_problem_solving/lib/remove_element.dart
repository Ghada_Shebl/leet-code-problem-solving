class RemoveElement{
   int removeSpecificElement(List<int> nums, int val) {
    int counter = 0;
    for (int i = 0; i < nums.length; i++){
      if (nums[i] != val){
        continue;
      }
      for (int j = i+1; j < nums.length; j++){
        if (nums[j] == val){
          continue;
        }
        int temp = nums[j];
        nums[j] = nums[i];
        nums[i] = temp;
      }
    }
    for (int i = 0; i<nums.length; i++){
      if (nums[i]!=val){
        counter++;
      }
    }
    return counter;
  }
}