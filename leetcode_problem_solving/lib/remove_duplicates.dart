class ListNode {
  int val;
  ListNode? next;
  ListNode([this.val = 0, this.next]);
  }

class RemoveDuplicates {
  ListNode? deleteDuplicates() {
    ListNode? head = ListNode(1, ListNode(1, ListNode(2)));
    ListNode? lastNode = head;
    while (head != null){
      if (head.val != lastNode?.val){
        lastNode?.next = head;
        lastNode = lastNode?.next;
      }
      head = head.next;
    }
    lastNode?.next = null;
    return head;
  }
}