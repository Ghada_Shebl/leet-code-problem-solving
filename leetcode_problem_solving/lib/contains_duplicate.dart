import 'dart:collection';

class ContainsDuplicate{
  bool result(List<int> nums){
    HashSet hashSet = HashSet();
    for (int i = 0; i < nums.length; i++){
      if (hashSet.contains(nums[i])){
        return true;
      }
      hashSet.add(nums[i]);
    }
    return false;
  }
}