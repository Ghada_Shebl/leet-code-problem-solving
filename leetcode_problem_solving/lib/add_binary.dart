class AddBinary{
    String add(String a, String b) {
      a = a.split('').reversed.join();
      b = b.split('').reversed.join();

      int carry = 0;
      int temp;
      int number1;
      int number2;
      String result = "";

      int length = a.length > b.length? a.length: b.length;

      for (int i = 0; i < length; i++){
        number1 = i < a.length ? int.parse(a[i]) : 0;
        number2 = i < b.length ? int.parse(b[i]) : 0;
        temp = number1 + number2 + carry;
        carry = temp~/2;
        temp = temp%2;
        result = temp.toString() + result;
      }
      if (carry == 1) result = carry.toString()+result;
      return result;

    }
}