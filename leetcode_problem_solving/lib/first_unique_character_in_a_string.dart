class FirstUniqueCharacter{
  int getIndex(String s){
    int index = -1;
    for (int i = 0; i < s.length; i++){
      index = i;
      for (int j = 0; j < s.length; j++){
        if (i == j)continue;
        if (s[i] == s[j]) {
          index = -1;
          break;
        }
      }
      if (index != -1){
        return index;
      }
    }

    return index;
  }
}