 class ListNode {
  int val;
  ListNode? next;
  ListNode([this.val = 0, this.next]);
  }

class MergeLists {
  ListNode? mergeTwoLists() {
    ListNode? list1 = ListNode(1, ListNode(2, ListNode(4)));
    ListNode? list2 = ListNode(1, ListNode(2, ListNode(3)));

    ListNode resultHead = ListNode(-1);
    ListNode? dummy = resultHead;

     while (list1 != null || list2 != null){
      if (list2 == null || list1!.val <= list2.val){
        dummy?.next = ListNode(list1!.val);
        list1 = list1!.next;
      }

      else if  (list1 == null || list2.val < list1.val){
       dummy?.next = ListNode(list2.val);
        list2 = list2.next;
      }
      dummy = dummy?.next;
           
    }
    return resultHead.next;
  }
}