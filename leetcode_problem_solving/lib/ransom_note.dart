import 'dart:collection';

class RansomNote{
  bool canConstruct(String ransomNote, String magazine) {
        Map <String,int> map = HashMap();
        for (int i = 0; i < magazine.length; i++){
          if (map.containsKey(magazine[i])){
            map[magazine[i]] = map[magazine[i]]!+1;
            continue;
          }
          map.putIfAbsent(magazine[i], () => 1);
        }


        for (int i = 0; i < ransomNote.length; i++){
          if (!map.containsKey(ransomNote[i])){
            return false;
          }
          if (map[ransomNote[i]] == 0){
            return false;
          }
          map[magazine[i]] = map[magazine[i]]!-1;
        }
        return true;
    }
}