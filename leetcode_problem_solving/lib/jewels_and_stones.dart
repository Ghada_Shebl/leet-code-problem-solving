class JewelsAndStones{
  int getCount(String jewels, String stones){
        int counter = 0;
        for (int i = 0; i < stones.length; i++){
            if (jewels.contains(stones[i])){
                counter++;
            }
        }
        return counter;
  }
}