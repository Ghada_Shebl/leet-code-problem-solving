class RomanToInteger{
    int result(String s) {
        Map <String,int> map = {
          'I':1,
          'V':5,
          'X':10,
          'L':50,
          'C':100,
          'D':500,
          'M':1000
        };
        int res = 0;
        for (int i = 0; i < s.length-1; i++){
          int current = map[s[i]]??0;
          int next = map[s[i+1]]??0;
          if (current >=next){
            res+=current;
            continue;
          }
          res-=current;
        }
        res+=map[s[s.length-1]]??0;
        print ("Integer of $s = $res");
        return res;
    }
}
